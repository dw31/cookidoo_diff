#!/bin/bash

MY_EMAIL="##TBD##"
if [ "$MY_EMAIL" = "##TBD##" ];then
  echo "error: SET MY_EMAIL VAR..."
  exit 1
fi

if [ $# -eq 1 ]; then
  python cookidoo.py "$1"
  cat collections_cur.json | grep "name" | grep -v "                    \"name" | sort | cut -d "\"" -f4 > tmp.json
  if [ `diff tmp.json collections_saved.json | wc -l` -eq 0 ]; then
    echo "OK NO DIFF, NOTHING DONE ("`cat collections_saved.json | wc -l`"collections)" | mail -s "Cookido Differ result: NO DIFF" "$MY_EMAIL" -A collections_saved.json
  else
    echo "KO DIFF, CHANGE SAVED ! SEE DIFF AND ATTACHMENTS:
    "`cat collections_saved.json | wc -l`" vs "`cat tmp.json | wc -l`" collections
    -------------- DIFF ------------
    "`diff collections_saved.json tmp.json` | mail -s "Cookido Differ result: KO DIFFS FOUND" "$MY_EMAIL" -A tmp.json -A collections_saved.json
    mv tmp.json collections_saved.json
    git add collections_saved.json
    git commit -m "auto save cookido.sh"
    git push origin master
  fi
else
  echo "usage: ./cookido.sh [authorization bearer]"
fi
rm -f tmp.json collections_cur.json
