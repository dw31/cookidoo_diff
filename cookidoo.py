#!/bin/python

import os
import re, sys, json, urllib2, argparse

parser = argparse.ArgumentParser(description='cookidoo')
parser.add_argument('authorization', type=str, help='authorization bearer')
args = parser.parse_args()

try:
    headers = {"Authorization": args.authorization}
    request = urllib2.Request('https://cookidoo.fr/vorwerkApiV2/apiv2/recipeCollection?searchTerm=&sort=0&limit=1000', headers=headers)
    collections = json.loads(urllib2.urlopen(request).read())
except Exception, e:
    if e.code == 401: print 'INVALID BEARER'
    else: print str(e)
    exit(2)

out_file = open('collections_cur.json', 'w')
out_file.write(json.dumps(collections, indent=4))
out_file.close()
